#include <stdio.h>

float addition(float a, float b) {
    return a + b;
}

float soustraction(float a, float b) {
    return a - b;
}

float multiplication(float a, float b) {
    return a * b;
}

float division(float a, float b) {
    if(b == 0) {
        printf("On ne peut pas diviser par 0");
        return 0;
    }
    return a / b;
}