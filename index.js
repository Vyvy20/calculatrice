var buttonsContainer = document.getElementById('buttons');

for (var i = 0; i <= 9; i++) {
    var button = document.createElement('button');
    button.textContent = i;
    button.addEventListener('click', function() {
        addToDisplay(this.textContent);
    });
    buttonsContainer.appendChild(button);
}

var operators = ['+', '-', '*', '/'];
operators.forEach(function(operator) {
    var button = document.createElement('button');
    button.textContent = operator;
    button.addEventListener('click', function() {
        addToDisplay(operator);
    });
    buttonsContainer.appendChild(button);
});

var equalsButton = document.createElement('button');
equalsButton.textContent = '=';
equalsButton.addEventListener('click', calculateResult);
buttonsContainer.appendChild(equalsButton);

var clearButton = document.createElement('button');
clearButton.textContent = 'C';
clearButton.addEventListener('click', clearDisplay);
buttonsContainer.appendChild(clearButton);

function addToDisplay(value) {
    var display = document.getElementById('display');
    display.value += value;
}

function calculateResult() {
    var display = document.getElementById('display');
    try {
        var result = eval(display.value);
        display.value = result;
    } catch (error) {
        display.value = 'Error';
    }
}

function clearDisplay() {
    var display = document.getElementById('display');
    display.value = ''; 
}
